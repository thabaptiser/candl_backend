import uuid
import random
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean, DateTime
from sqlalchemy.orm import sessionmaker
from geoalchemy2 import Geometry
import datetime

Base = declarative_base()
engine = create_engine('postgresql://postgres:corsica99@localhost/candl', echo=True)
Session = sessionmaker(bind=engine)
session = Session()

class Event(Base):
  __tablename__ = 'Event'
  id = Column(Integer, primary_key=True, autoincrement=True)
  name = Column(String)
  geom = Column(Geometry('POLYGON'))
  start = Column(DateTime)
  end = Column(DateTime)

class User(Base):
  __tablename__ = 'User'
  facebook_id = Column(Integer, primary_key=True)
  name = Column(String)
  sex = Column(Boolean) 
  wants_men = Column(Boolean)
  wants_women = Column(Boolean)
  pictures = Column(String)
  facebook_auth = Column(String)
  event_id = Column(Integer)

class Swipe(Base):
  __tablename__ = 'Swipe'
  id = Column(Integer, primary_key=True, autoincrement=True)
  event_id = Column(Integer, index=True)
  p1 = Column(Integer, index=True)
  p2 = Column(Integer, index=True)
  swipe_type = Column(Boolean)
  
def create_models():
  Event.__table__.create(engine)
  User.__table__.create(engine)
  Swipe.__table__.create(engine)

def populate_db():
  for i in range(1000):
    session.add(User(name=uuid.uuid4(), sex=random.choice([True, False]), wants_men=random.choice([True, False]), wants_women=random.choice([True, False]), pictures='', facebook_auth='', event_id=0))
  session.add(Event(name='candl', geom='POLYGON((-79.953447 40.458685,-79.944134 40.458783,-79.941259 40.451795,-79.953662 40.449574,-79.953447 40.458685))', start = datetime.datetime.now(), end=datetime.datetime(2099, 1,1)))

#create_models()
#populate_db()
