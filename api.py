from database import *
from flask import Flask, jsonify
from functools import wraps
from flask import request, abort, Response

app = Flask(__name__)

def check_auth(facebook_id, auth):
    """This function is called to check if a username /
    password combination is valid.
    """
    return session.query(User).get(facebook_id).facebook_auth == auth

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
  @wraps(f)
  def decorated(*args, **kwargs):
      if not request.json:
          abort(400)
      facebook_id, facebook_auth = request.json['facebook_id'], request.json['auth_token']
      if not check_auth(facebook_id, facebook_auth):
          return authenticate()
      return f(*args, **kwargs)
  return decorated

@requires_auth
@app.route('/api/find_matches', methods = ['GET'])
def find_matches():
  if not request.json:
    abort(400)
  facebook_id = request.json['facebook_id']
  event = request.json['event_id']
  curr_user = session.query(User).filter(User.facebook_id==facebook_id)
  if curr_user.sex == False:
    if curr_user.wants_women and curr_user.wants_men:
      matches = session.query(User).filter(User.event_id==curr_user.event_id).filter(User.wants_women==True).all()
    elif curr_user.wants_women:
      matches = session.query(User).filter(User.event_id==curr_user.event_id).filter(User.sex==False).filter(User.wants_women==True).all()
    elif curr_user.wants_men:
      matches = session.query(User).filter(User.event_id==curr_user.event_id).filter(User.sex==True).filter(User.wants_women==True).all()
  elif curr_user.sex == True:
    if curr_user.wants_women and curr_user.wants_men:
      matches = session.query(User).filter(User.event_id==curr_user.event_id).filter(User.wants_men==True).all()
    elif curr_user.wants_women:
      matches = session.query(User).filter(User.event_id==curr_user.event_id).filter(User.sex==False).filter(User.wants_men==True).all()
    elif curr_user.wants_men:
      matches = session.query(User).filter(User.event_id==curr_user.event_id).filter(User.sex==True).filter(User.wants_men==True).all()
  ret = []
  for m in matches:
    ret.append({'id': m.facebook_id, 'pictures': m.pictures, 'name': m.name})
  return jsonify(ret)

@app.route('/api/find_events', methods = ['GET'])
def find_events():
  if not request.json:
    abort(400)
  lon, lat = request.json['location']
  events = session.query(Event).filter(func.ST_Contains(Event.geom, 'POINT({long} {lat})'.format(lon=lon, lat=lat))).all()
  ret = []
  for event in events:
     ret.append({'id': event.id, 'name': event.name})
  return jsonify(ret)

@app.route('/api/login', methods = ['GET'])
def login():
  if not request.json:
    abort(400)
  payload = {'client_id': FACEBOOK_APP_ID, 'client_secret': FACEBOOK_APP_SECRET, 'grant_type': 'client_credentials'}
  app_token = request.get("https://graph.facebook.com/v2.7/oauth/access_token", params = payload).json()['access_token']
  payload = {'client_id': FACEBOOK_APP_ID, 'redirect_uri': '', 'client_secret': FACEBOOK_APP_SECRET, 'code': request.json['code']}
  r = requests.get("https://graph.facebook.com/v2.7/oauth/access_token", params=payload)
  payload = {'input_token': r.json()['access_token'], 'access_token': app_token}
  facebook_id = request.get("graph.facebook.com/debug_token").json()['data']['user_id']
  try:
    access_token = r.json()['access_token']
    user = session.query(User).get(facebook_id)
    if not user:
      return jsonify({'error': 'user does not exist'})
    user.facebook_auth = access_token
    session.commit()
    return jsonify({'facebook_id': facebook_id, 'access_token': access_token})
  except KeyError:
    abort(400)
  abort(400)

@app.route('/list', methods = ['GET'])
def list():
  ret = []
  for i in session.query(User).all():
    ret.append(i.name)
  return jsonify(ret)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
